﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PreStartController : MonoBehaviour
{
    public TMP_Text preStartText; //The text element to say either "Tap to Jump" or "Space to Jump"

    private LevelGenerator levelGenerator; //Used for "pausing" the game before the player is ready
    private float _speed; //Store the default speed of the game so we can reset it when the player is ready
    private float _maxSpeed; //Store the max speed of the game so we can reset it when the player is ready

    // Start is called before the first frame update
    void Start()
    {
        levelGenerator = FindObjectOfType<LevelGenerator>();

        //Get the default and max speed of the game
        _speed = levelGenerator.speed;
        _maxSpeed = levelGenerator.maxSpeed;
        //"Pause" the game
        levelGenerator.speed = 0;
        levelGenerator.maxSpeed = 0;

#if UNITY_ANDROID || UNITY_IOS
        //If we're on a mobile device, say "Tap to Jump"
        preStartText.text = "Tap to Jump";
        //Otherwise, say "Space to Jump"
# else
        preStartText.text = "Space to Jump";
#endif
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID || UNITY_IOS
        //If the user touches the screen, start the game
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
                _Start();
        }
#else
        //If the user presses the space bar, start the game
        if (Input.GetKeyDown(KeyCode.Space))
            _Start();
#endif
    }

    /// <summary>
    /// Start the game by resetting the default and max speed of the game
    /// </summary>
    void _Start()
    {
        levelGenerator.speed = _speed;
        levelGenerator.maxSpeed = _maxSpeed;
        Destroy(gameObject); //We don't need this anymore
    }
}
