﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the spawn of segments
/// </summary>
public class LevelGenerator : MonoBehaviour
{
    public int initialSegments; //How many segments to spawn at the start
    public float speed; //The speed of the segments
    public float maxSpeed; //The maximum speed of the game
    public GameObject[] levelParts; //The segments we can choose from

    public float fruitSpawnChance; //How likely the fruit is going to spawn
    public GameObject fruitPrefab;

    public float heartOrBoxSpawnChance; //How likely a heart OR box is going to spawn
    public float heartSpawnChance; //Of the above chance, should we spwan a heart. Otherwise we spawn a box
    public GameObject heartPrefab;

    public GameObject boxPrefab;

    [SerializeField] private List<GameObject> environment = new List<GameObject>(); //A list of every segment

    private void Start()
    {
        UpdateEnvironmentList(); //Update the list of segments just in case we started with some already in the scene
        SpawnLevelPart(initialSegments); //Spawn 10 segments to start
    }

    private void Update()
    {
        //Speed up the game over time
        if (speed < maxSpeed)
            speed += Time.deltaTime / 5;
    }

    /// <summary>
    /// Update the list of segments
    /// </summary>
    void UpdateEnvironmentList()
    {
        environment.Clear(); //Clear the list to remove the old segments
        //Add all the children of this GO to the environment list
        foreach (Transform child in transform)
        {
            environment.Add(child.gameObject);
        }
    }

    #region SpawnLevelPart overloads
    /// <summary>
    /// Spawn a new segment at the end of the line
    /// </summary>
    public void SpawnLevelPart()
    {
        SpawnLevelPart(environment[environment.Count - 1].transform.Find("PartEnd").transform, true);
    }

    /// <summary>
    /// Spawn a new segment at a given transform
    /// </summary>
    /// <param name="trans">The transform of where to spawn a new segment</param>
    public void SpawnLevelPart(Transform trans, bool pickups)
    {
        SpawnLevelPart(trans.position, pickups);
    }

    /// <summary>
    /// Spawn a new segment at a given Vector2 position
    /// </summary>
    /// <param name="pos">The vector2 to spawn the segment at</param>
    public void SpawnLevelPart(Vector2 pos, bool pickups)
    {
        GameObject tempSegment = Instantiate(levelParts[Random.Range(0, levelParts.Length)], pos, Quaternion.identity, transform); //Instantiate a new segment at the given pos
        tempSegment.GetComponent<LevelPartController>().spawnPickups = pickups;
        UpdateEnvironmentList(); //Update the environment list
    }

    /// <summary>
    /// Spawn a given number of segments starting at the end
    /// </summary>
    /// <param name="numToSpawn">The number of segments to spawn</param>
    public void SpawnLevelPart(int numToSpawn)
    {
        for (int i = 0; i < numToSpawn; i++)
        {
            //Are there already segments
            if (environment.Count > 0)
            {
                SpawnLevelPart(environment[environment.Count - 1].transform.Find("PartEnd"), false); //Spawn a segment at the end of the line
            }
            //No segments so start at the origin
            else
            {
                SpawnLevelPart(Vector2.zero, false);
            }
        }
    }
    #endregion
}
