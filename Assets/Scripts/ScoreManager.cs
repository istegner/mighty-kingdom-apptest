﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// The data we want to save
/// </summary>
[Serializable]
public class SaveData
{
    public int _fruit;
    public float _highestDistanceTraveled;
}

public class ScoreManager : MonoBehaviour
{
    static bool debug = true;

    public string saveDataName; //The name of the save file
    public static int fruit; //How many fruit the player has
    public static int hearts = 3; //How many hearts the player has
    public static float highestTraveled; //The highest the player has traveled

    private static string saveDataPath; //The path of the save file

    private void Start()
    {
        Debug.LogWarning(saveDataPath); //So I know where it is
    }

    /// <summary>
    /// Save the player's stats
    /// </summary>
    public static void Save()
    {
        if (debug) Debug.Log("Saving Game");

        FileStream file;
        file = File.Open(saveDataPath, FileMode.OpenOrCreate);
        BinaryFormatter bf = new BinaryFormatter();
        SaveData data = new SaveData
        {
            _fruit = fruit,
            _highestDistanceTraveled = highestTraveled
        };

        bf.Serialize(file, data);
        file.Close();

        if (debug) Debug.Log("Done saving");
    }

    /// <summary>
    /// Load the player's stats
    /// </summary>
    public void Load()
    {
        if (debug) Debug.Log("Loading Game");

        if (File.Exists(saveDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(saveDataPath, FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();

            fruit = data._fruit;
            highestTraveled = data._highestDistanceTraveled;

            if (debug) Debug.Log("Done Loading");
        }
        else
            if (debug) Debug.Log("No save data to load");
    }

    /// <summary>
    /// Check and set a new highest distance traveled
    /// </summary>
    /// <param name="toSetTo">The distance to check against the highest</param>
    public static void SetHighestTraveled(float toSetTo)
    {
        if (toSetTo > highestTraveled)
            highestTraveled = toSetTo;
    }

    /// <summary>
    /// Delete the save file
    /// </summary>
    [ContextMenu("Delete Save")]
    public void DeleteSaveData()
    {
        if (File.Exists(saveDataPath))
            File.Delete(saveDataPath);
    }

    private void OnEnable()
    {
        //Make sure there's only 1 ScoreManager
        if (FindObjectsOfType<ScoreManager>().Length > 1)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject); //Keep this a singleton
        saveDataPath = Path.Combine(Application.persistentDataPath, saveDataName); //Figure out where to save/load stuff
        Load(); //Load the player's stats
    }

    /// <summary>
    /// If the game becomes unfocused, save the player's stats just in case it closes
    /// </summary>
    /// <param name="pause"></param>
    private void OnApplicationPause(bool pause)
    {
        if (pause)
            Save();
    }
}
