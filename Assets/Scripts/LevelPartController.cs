﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlls the movement and destruction of environment segments
/// </summary>
public class LevelPartController : MonoBehaviour
{
    public Transform[] pickUpSpawnPoints; //The locations of where pickups/obstacles can spawn
    public bool spawnPickups; //Should we spawn pickups/obstacles when created

    private LevelGenerator levelGenerator; //Contains the speed we move at

    private void Start()
    {
        levelGenerator = FindObjectOfType<LevelGenerator>();

        if (spawnPickups)
        {
            //Randomly spawn pickups/obstacles
            for (int i = 0; i < pickUpSpawnPoints.Length; i++)
            {
                float rand = Random.Range(0.0f, 1.0f);
                if (rand >= levelGenerator.heartOrBoxSpawnChance)
                {
                    float rand2 = Random.Range(0.0f, 1.0f);
                    if (rand2 >= levelGenerator.heartSpawnChance)
                        Instantiate(levelGenerator.heartPrefab, pickUpSpawnPoints[i].position, Quaternion.identity, transform);
                    else
                        Instantiate(levelGenerator.boxPrefab, pickUpSpawnPoints[i].position, Quaternion.identity, transform);

                }
                else if (rand >= levelGenerator.fruitSpawnChance)
                {
                    Instantiate(levelGenerator.fruitPrefab, pickUpSpawnPoints[i].position, Quaternion.identity, transform);
                }
            }
        }
    }

    private void Update()
    {
        //Move the segment left at levelGenerators speed
        transform.position -= new Vector3(levelGenerator.speed * Time.deltaTime, transform.position.y, transform.position.z);
    }

    /// <summary>
    /// If we collide with the world end, spawn a new segment and destroy ourselves
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.gameObject.name);
        if (other.CompareTag("PartEnd"))
        {
            FindObjectOfType<LevelGenerator>().SpawnLevelPart();
            Destroy(gameObject);
        }
    }
}
