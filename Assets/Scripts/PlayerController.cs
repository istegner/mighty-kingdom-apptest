﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    public bool debug; //Should we show debug logs

    public float jumpForce; //How much force to apply to jump
    public float maxJumpTime; //How long the jumpForce will be applied while the jump key is pressed
    public float groundedOffset; //How far below the player to check for ground

    public float distanceTraveled = 0.0f; //How far the player has traveled this run

    public GameObject retryPanel; //The UI panel with the retry & exit buttons

    [SerializeField] private LayerMask platformLayer; //The layer the platforms are in
    private Rigidbody2D rb; //The players rigidbody
    private BoxCollider2D playerCollider; //The player box collider
    private LevelGenerator levelGenerator; //Needed for the speed of the world
    private bool dead = false; //Are we dead? True to stop spamming the Die() function
    [SerializeField] private float currentJumpTime; //How long has the player been jumping
    [SerializeField] private bool canJump = true; //If the currentJumpTime > maxJumpTime, disallow the player to jump
    private bool isJumping = false; //Is the player currently jumping

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<BoxCollider2D>();
        levelGenerator = FindObjectOfType<LevelGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Are we on the ground
        if (IsGrounded())
        {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
            //Jump on space bar down
            if (Input.GetKeyDown(KeyCode.Space))
                isJumping = true;
#else
            //If the player touches the screen
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                //If the player only just touched the screen
                if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    isJumping = true;
            }
#endif
        }

        //If the player releases the space bar or stops touching the screen
        //Stop jumping
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Space))
            ResetJump();
#else
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended)
                ResetJump();
        }
#endif
        if (!dead && ScoreManager.hearts <= 0)
            Die();

        distanceTraveled += (levelGenerator.speed * Time.deltaTime) / 5; //Calculate how far the player has traveled. Divide 5 because of scale
    }

    private void FixedUpdate()
    {
        if (isJumping && canJump)
            Jump();
    }

    /// <summary>
    /// Add the jump force onto the players rigidbody
    /// </summary>
    void Jump()
    {
        //If we haven't reached our max jump time
        //Add the jump force
        if (currentJumpTime < maxJumpTime)
        {
            isJumping = true;
            currentJumpTime += Time.deltaTime;
            rb.velocity = Vector2.up * jumpForce;
        }
        //Else, stop jumping
        else
            canJump = false;
    }

    /// <summary>
    /// Reset all the jump variables
    /// </summary>
    void ResetJump()
    {
        canJump = true;
        currentJumpTime = 0;
        isJumping = false;
    }

    /// <summary>
    /// Do a boxcast to see if the player is close enough to a platform to allow them to jump
    /// </summary>
    /// <returns>True if on a platform</returns>
    bool IsGrounded()
    {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(playerCollider.bounds.center, playerCollider.bounds.size, 0f, Vector2.down, groundedOffset, platformLayer);
        return raycastHit2D.collider != null;
    }

    /// <summary>
    /// The player has died
    /// </summary>
    void Die()
    {
        if (debug) Debug.Log("Player Died");
        dead = true;
        GetComponent<BoxCollider2D>().enabled = false;
        ScoreManager.Save(); //Save the game just in case the player closes the game in rage
        retryPanel.SetActive(true); //Ask if the player wants to retry or quit
        Time.timeScale = 0.0f; //Freeze the game
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Has the player gone off the map
        if (other.CompareTag("PlayerDeath"))
        {
            ScoreManager.SetHighestTraveled(distanceTraveled);
            ScoreManager.hearts = 0;
        }
        //Has the player picked up a fruit
        else if (other.CompareTag("Pickup"))
        {
            ScoreManager.fruit++;
            Destroy(other.gameObject);
        }
        //Has the player picked up a heart
        else if (other.CompareTag("Heart"))
        {
            ScoreManager.hearts++;
            Destroy(other.gameObject);
        }
        //Has the player collided with a box
        else if (other.gameObject.CompareTag("Obstacle"))
        {
            ScoreManager.hearts--;
            other.GetComponent<Collider2D>().enabled = false;
        }
    }
}
