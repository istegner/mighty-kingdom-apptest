﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles updating the main UI
/// </summary>
public class UIController : MonoBehaviour
{
    public TMP_Text heartsText; //The text field for the number of hearts the player has
    public TMP_Text distanceText; //The text field for how far the player has traveled this run
    public TMP_Text fruitText; //The text field for the number of fruit the player has collected ( of all time)
    public TMP_Text highestText; //The text field for the highest distance the player has traveled

    private PlayerController playerController; //Used for getting the current distance traveled

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    /// <summary>
    /// Update all the UI text elements
    /// </summary>
    void Update()
    {
        if (heartsText)
            heartsText.text = ScoreManager.hearts.ToString();
        if (distanceText && playerController)
            distanceText.text = playerController.distanceTraveled.ToString("0.0") + "m";
        if (fruitText)
            fruitText.text = ScoreManager.fruit.ToString();
        if (highestText)
            highestText.text = ScoreManager.highestTraveled.ToString("0.0") + "m";
    }

    /// <summary>
    /// The "Retry" button has been pressed
    /// </summary>
    public void Retry()
    {
        Time.timeScale = 1.0f; //Set time scale back to 1
        ScoreManager.hearts = 3; //Reset the number of hearts the player has to begin with
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //Reload the scene
    }

    /// <summary>
    /// Close the game
    /// </summary>
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
